//Lista de items disponibles
let listaItems = new Array();

//Lista de items agregados al carrito
let carritoCompras = new Array();

let sesionIniciada = false;

class Producto{
    id;
    nombre;
    precio;

    constructor(id, nombre, precio){
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }
}

//Agregar todos los items a la lista
function cargarItems(){
    listaItems.push(new Producto(0,"Juego de llaves combinadas x5", 2500));
    listaItems.push(new Producto(1,"Juego de llaves combinadas x10", 4500));
    listaItems.push(new Producto(2,"Juego de llaves combinadas x15", 7000));
    listaItems.push(new Producto(3,"Llaves tubo x20", 1000));
    listaItems.push(new Producto(4,"Llaves tubo x50", 2500));
    listaItems.push(new Producto(5,"Kit destornilladores x10", 1800));
    listaItems.push(new Producto(6,"Kit destornilladores x20", 3500));
    listaItems.push(new Producto(7,"Paño microfibra 40x40", 100));
    listaItems.push(new Producto(8,"Paño microfibra 60x40", 150));
    listaItems.push(new Producto(9,"Compresor 50 litros 2.5hp", 20000));
    listaItems.push(new Producto(10,"Compresor 100 litros 2.5hp", 30000));
    listaItems.push(new Producto(11,"Compresor 200 litros 3hp", 60000));
    listaItems.push(new Producto(12,"Kit extractor de tapizados x3", 3000));
    listaItems.push(new Producto(13,"Kit extractor de tapizados x6", 5000));
    listaItems.push(new Producto(14,"Pulidora 1000w", 13000));
    listaItems.push(new Producto(15,"Pulidora 1600w", 16000));
    listaItems.push(new Producto(16,"Amoladora 600w", 4000));
    listaItems.push(new Producto(17,"Amoladora 820w", 6000));
    listaItems.push(new Producto(18,"Shampoo 1lt", 1000));
    listaItems.push(new Producto(19,"Shampoo 5lt", 3000));
    listaItems.push(new Producto(20,"Hidrolavadora Hogar", 12000));
    listaItems.push(new Producto(21,"Hidrolavadora Premium", 50000));

    sesionIniciada = localStorage.getItem('sesion');
    if(sesionIniciada == null){
        sesionIniciada = false;
    }
    comprobarSesion();
    
    mostrarItems();
    cargarCarrito();
}

//Mostrar los items por pantalla
function mostrarItems(){
    for (item of listaItems){
        $('#item_list').append("<section>" + "<h3>"+item.nombre+"</h3><br/><p>$"+item.precio+"</p>"+"<button onclick=\"(agregarItem('"+item.id+"'))\">Agregar al Carrito</button>"+"<section/>");
    }
    
}

//Agregar el producto seleccionado al carrito de compras
function agregarItem(id){
    item = listaItems[id];
    let opcion = confirm("¿Agregar " + item.nombre + " al Carrito de Compras?");
    if (opcion){
        carritoCompras.push(item);
        $('.alert_positivo').fadeIn();
        setTimeout(function(){
            $('.alert_positivo').fadeOut();}
            ,1000);
    }
}

function cargarCarrito(){
    carritoCompras.length = 0;
    let objSerialized = localStorage.getItem('cart');
    carritoCompras = JSON.parse(objSerialized);
    if(carritoCompras == null){
        carritoCompras = new Array();
    }
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function guardarCarrito(){
    let objSerialized = JSON.stringify(carritoCompras);
    localStorage.setItem('cart', objSerialized);
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function comprobarSesion(){
    if(sesionIniciada){
        $(".sesion_iniciada").css('display', 'inline-flex');
        $(".sesion_cerrada").css('display', 'none');
    }
    else{
        $(".sesion_iniciada").css('display', 'none');
        $(".sesion_cerrada").css('display', 'inline-flex');
    }
}

function cerrarSesion(){
    sesionIniciada = false;
    comprobarSesion();
    localStorage.setItem('sesion', false);
}
