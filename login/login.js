let usuarios = new Array();

class Usuario{
    email;
    password;
    nombre;
    apellido;

    constructor(email, password, nombre, apellido){
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
    }
}

function iniciarSesion(){
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    
    console.log(email);
    console.log(password);

    for(user of usuarios){
        if(user.email == email){
            if(user.password == password){
                $('.alert_positivo').fadeIn();
                setTimeout(function(){
                    $('.alert_positivo').fadeOut();}
                    ,1000);
                alert("Sesion Iniciada");                
                return true;                    
            }
            else{
                alert("Contraseña invalida");
                return false;
            }
        }
    }
    alert("Usuario no registrado");
}

function cargarUsuarios(){
    //usuarios = localStorage.getItem('usuarios');
    let objSerialized = localStorage.getItem('usuarios');
    usuarios = JSON.parse(objSerialized);
    if(usuarios == null){
        usuarios = new Array();
    }

    for(user of usuarios){
        console.log(user.email);
    }
    /*
    usuarios.push(new Usuario('12345@gmail.com','12345','Pepito','Pepe'));
    usuarios.push(new Usuario('abcdef@gmail.com','abcdef','ABC','DEF'));
    usuarios.push(new Usuario('tai@gmail.com','tai','Tai','G'));
    console.log(usuarios.length);*/
}