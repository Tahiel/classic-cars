class Usuario{
    email;
    password;

    constructor(email, password){
        this.email = email;
        this.password = password;
    }
}

function mostrarCamposTarjeta()
{
    document.getElementById("first_method").style.display='';
    document.getElementById("second_method").style.display = "none";
    document.getElementById("third_method").style.display = "none";
}

function mostrarCamposCuentaBanc()
{
    document.getElementById("first_method").style.display = "none";
    document.getElementById("second_method").style.display='';
    document.getElementById("third_method").style.display = "none";
}

function mostrarCamposMP()
{
    document.getElementById("first_method").style.display = "none";
    document.getElementById("second_method").style.display = "none";
    document.getElementById("third_method").style.display='';
}

const expresiones = {
    nombre : /^[a-zA-Z\s]{1,30}/,
    pass: /^[a-zA-Z0-9]{8}$/,
    correo : /^[a-zA-Z0-9.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    numeros : /^\d{7,14}$/
    
}

function validation()
{
    var name = document.getElementById("name").value;
    var lastname = document.getElementById("lastname").value;
    var address = document.getElementById("address").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;



    if( ! expresiones.nombre.test(name))
    {
        alert("Nombre no válido")
        return;
    }

    if( ! expresiones.nombre.test(lastname))
    {
        alert("Apellido no válido")
        return;
    }

    if( ! expresiones.nombre.test(address))
    {
        alert("Dirección no válida")
        return ;
    }

    if( ! expresiones.correo.test(email))
    {
        alert("Correo no válido")
        return;
    }

    if( ! expresiones.pass.test(password))
    {
        alert("Contraseña no válida")
        return;
    }

    let objSerialized = localStorage.getItem('usuarios');
    let listaUsuarios = JSON.parse(objSerialized);
    if(listaUsuarios == null){
        listaUsuarios = new Array();
    }
    let user = new Usuario(email, password);
    for(usuario of listaUsuarios){
        if(usuario.email == email){
            alert('Este usuario ya esta registrado');
            return;
        }
    }
    $('.alert_positivo').fadeIn();
    setTimeout(function(){
        $('.alert_positivo').fadeOut();}
        ,1000);
    listaUsuarios.push(user);
    objSerialized = JSON.stringify(listaUsuarios);
    localStorage.setItem('usuarios', objSerialized);
}

function limpiarFormulario() {
    //localStorage.clear();
    document.getElementById("registration").reset();
}