let listaServicios = new Array();
let carritoCompras = new Array();
let sesionIniciada = false;

class Servicio{
    id;
    nombre;
    precio;
    fecha;
    horario;

    constructor(id, nombre, precio){
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.fecha = '2020-12-02';
        this.horario = '12:00';
    }

    setFecha(fecha) {
        this.fecha = fecha;
    }

    setHorario(horario){
        this.horario = horario;
    }
}

function cargarServicios(){
    listaServicios.push(new Servicio(0, "Chapa y Pintura", 20000));
    listaServicios.push(new Servicio(1, "Tapicería", 8000));
    listaServicios.push(new Servicio(2, "Vidriería", 15000));
    listaServicios.push(new Servicio(3, "Reparación de Plásticos", 5000));
    listaServicios.push(new Servicio(4, "Restauración de Interiores", 7500));
    listaServicios.push(new Servicio(5, "Gomería", 12000));
    listaServicios.push(new Servicio(6, "Mecánica Integral", 30000));
    listaServicios.push(new Servicio(7, "Lubricentro y Detailing completo", 6000));
}

function mostrarServicios(){
    for(item of listaServicios){
        $('#service_list').append("<section>" + "<h3>"+item.nombre+"</h3><br/><p>$"+item.precio+"</p>"+"<input type='date' value='2020-12-02' class='fecha' onchange=\"(cambiarFechas('"+item.id+"'))\"><input type='time' value='12:00:00' class='horario' onchange=\"(cambiarHorarios('"+item.id+"'))\">"+"<button onclick=\"(agregarItem('"+item.id+"'))\">Agregar al Carrito</button>"+"<section/>");
    }
}

function agregarItem(id){
    let item = listaServicios[id];
    let reserva = new Servicio(-1, "Reserva Servicio "+ item.nombre, 50);
    reserva.setFecha(item.fecha);
    reserva.setHorario(item.horario);
    let opcion = confirm("¿Agregar " + item.nombre + " al Carrito de Compras?");
    if (opcion){
        carritoCompras.push(item);
        carritoCompras.push(reserva);
        $('.alert_positivo').fadeIn();
        setTimeout(function(){
            $('.alert_positivo').fadeOut();}
            ,1000);
    }
}

function cargarCarrito(){
    carritoCompras.length = 0;
    let objSerialized = localStorage.getItem('cart');
    carritoCompras = JSON.parse(objSerialized);
    if(carritoCompras == null){
        carritoCompras = new Array();
    }
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function guardarCarrito(){
    let objSerialized = JSON.stringify(carritoCompras);
    localStorage.setItem('cart', objSerialized);
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function iniciar(){
    cargarCarrito();
    cargarServicios();
    mostrarServicios();
    sesionIniciada = localStorage.getItem('sesion');
    if(sesionIniciada == null){
        sesionIniciada = false;
    }
    comprobarSesion();
}

function cambiarFechas(id) {
    let fechas = document.getElementById("service_list").querySelectorAll(".fecha");
    console.log(fechas[id].value);
    listaServicios[id].setFecha(fechas[id].value);
}

function cambiarHorarios(id){
    let horarios = document.getElementById("service_list").querySelectorAll(".horario");
    console.log(horarios[id].value);
    listaServicios[id].setHorario(horarios[id].value);
}

function comprobarSesion(){
    if(sesionIniciada){
        $(".sesion_iniciada").css('display', 'inline-flex');
        $(".sesion_cerrada").css('display', 'none');
    }
    else{
        $(".sesion_iniciada").css('display', 'none');
        $(".sesion_cerrada").css('display', 'inline-flex');
    }
}

function cerrarSesion(){
    sesionIniciada = false;
    comprobarSesion();
    localStorage.setItem('sesion', false);
}