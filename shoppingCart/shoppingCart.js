let carritoCompras = new Array();
let precioTotal = 0;
let metodosPago = new Array();
let sesionIniciada = false;

class Tarjeta{
    numero;
    vencimiento;
    codigo;
    nombre;

    constructor(numero, vencimiento, codigo, nombre){
        this.numero = numero;
        this.vencimiento = vencimiento;
        this.codigo = codigo;
        this.nombre = nombre;
    }   
}

class CuentaBancaria{
    numero;
    cbu;
    cuitl;

    constructor(numero, cbu, cuitl){
        this.numero = numero;
        this.cbu = cbu;
        this.cuitl = cuitl;
    }
}

class MercadoPago{
    email;
    contraseña;

    constructor(email, contraseña){
        this.email = email;
        this.contraseña = contraseña;
    }
}

function cargarCarrito(){
    carritoCompras.length = 0;
    let objSerialized = localStorage.getItem('cart');
    carritoCompras = JSON.parse(objSerialized);
    if(carritoCompras == null){
        carritoCompras = new Array();
    }
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function guardarCarrito(){
    let objSerialized = JSON.stringify(carritoCompras);
    localStorage.setItem('cart', objSerialized);
    for(item of carritoCompras){
        console.log(item.nombre);
    }
}

function mostrarCarrito(){
    sesionIniciada = localStorage.getItem('sesion');
    if(sesionIniciada == null){
        sesionIniciada = false;
    }
    comprobarSesion();
    cargarCarrito();
    if(carritoCompras.length == 0){
        $('#shopping_cart').append("<section><p>Su carrito esta vacio</p><section/>");
    }
    for(item of carritoCompras){
        if(item.fecha != undefined){
            $('#shopping_cart').append("<section>" + "<p>"+item.nombre+"<br/>"+ item.fecha +"  "+item.horario +"<br/><br/>$"+item.precio+"</p><section/>");
        }
        else{
            $('#shopping_cart').append("<section>" + "<p>"+item.nombre+"<br/><br/>$"+item.precio+"</p><section/>");
        }       
        precioTotal += item.precio;
    }
    $('#shopping_cart_total').append("<p>"+precioTotal+"</p>");


}

function vaciarCarrito(){
    carritoCompras.length = 0;
    precioTotal = 0;
    $('#shopping_cart').empty();
    $('#shopping_cart_total').empty();


    guardarCarrito();
    mostrarCarrito();

}

function cargarMetodos(){
    metodosPago.push(new Tarjeta("1234567890123456", "12/20", "123", "Pepe"));
    metodosPago.push(new CuentaBancaria("AR202020202020202020","2222222222222222222222", "99111111119"));
    metodosPago.push(new MercadoPago("pepe@gmail.com", "1234"));    
}

//Se muestra el formulario para realizar la compra
function pagar(){
    if(carritoCompras.length == 0){
        $('#error_carrito').fadeIn();
            setTimeout(function(){
                $('#error_carrito').fadeOut();}
                ,2000);
        return;
    }
    if(metodosPago.length == 0){
        cargarMetodos();
    }
    $('#form').submit(function(){ finalizarCompra(); return false; });
    $('#payment').css("display", "inline-block");
    $('#payBtn').css("display", "none");
    $('#emptyBtn').css("display", "none");
    $('#paymentMethods').empty();
    let select = document.getElementById('paymentMethods');
    for(metodo of metodosPago){
        if(metodo instanceof Tarjeta){
            let option = document.createElement("option");
            option.text = "Tarjeta " + metodo.numero;
            select.appendChild(option);
        }
        if(metodo instanceof CuentaBancaria){
            let option = document.createElement("option");
            option.text = "Cuenta " + metodo.numero;
            select.appendChild(option);
        }
        if(metodo instanceof MercadoPago){
            let option = document.createElement("option");
            option.text = "MercadoPago " + metodo.email;
            select.appendChild(option);
        }
    }
}

//Se cancela la compra y oculta el formulario
function cancelarPago(){
    $('#payment').css("display", "none");
    $('#payBtn').css("display", "inline-block");
    $('#emptyBtn').css("display", "inline-block");
}

function finalizarCompra(){

    if(document.getElementById('name').value == '' ||document.getElementById('name').value == '' ||document.getElementById('name').value == '' ||document.getElementById('name').value == '' ){
        return;
    }

    let option = document.getElementById('paymentMethods').selectedIndex;
    let codigo = document.getElementById('code').value;
    console.log(option);
    console.log(codigo);
    
    let selectedMethod = metodosPago[option];

    if(selectedMethod instanceof Tarjeta){
        if(selectedMethod.codigo == codigo){
            vaciarCarrito();
            $('.alert_positivo').fadeIn();
            setTimeout(function(){
                $('.alert_positivo').fadeOut();}
                ,2000);
            //alert("Gracias por su compra");
            cancelarPago();
        }
        else{
            $('#error_pago').fadeIn();
            setTimeout(function(){
                $('#error_pago').fadeOut();}
                ,2000);
            //alert("Error en la verificacion");
        }
    }
    if(selectedMethod instanceof CuentaBancaria){
        if(selectedMethod.cbu == codigo){
            vaciarCarrito();
            $('.alert_positivo').fadeIn();
            setTimeout(function(){
                $('.alert_positivo').fadeOut();}
                ,1000);
            cancelarPago();          
        }
        else{
            $('#error_pago').fadeIn();
            setTimeout(function(){
                $('#error_pago').fadeOut();}
                ,2000);
        }
    }
    if(selectedMethod instanceof MercadoPago){
        if(selectedMethod.contraseña == codigo){
            vaciarCarrito();
            $('.alert_positivo').fadeIn();
            setTimeout(function(){
                $('.alert_positivo').fadeOut();}
                ,1000);
            cancelarPago();
        }
        else{
            $('#error_pago').fadeIn();
            setTimeout(function(){
                $('#error_pago').fadeOut();}
                ,2000);
        }
    }
}

function comprobarSesion(){
    if(sesionIniciada){
        $(".sesion_iniciada").css('display', 'inline-flex');
        $(".sesion_cerrada").css('display', 'none');
    }
    else{
        $(".sesion_iniciada").css('display', 'none');
        $(".sesion_cerrada").css('display', 'inline-flex');
    }
}

function cerrarSesion(){
    sesionIniciada = false;
    comprobarSesion();
    localStorage.setItem('sesion', false);
}
